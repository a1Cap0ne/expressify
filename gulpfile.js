const gulp = require('gulp');
const typescript = require('gulp-typescript');

const tsProjectCore = typescript.createProject('core/tsconfig.json');

gulp.task('build:core', () => {
    return gulp.src('core/src/**/*.ts')
            .pipe(tsProjectCore())
            .pipe(gulp.dest('core/lib'))
    
})